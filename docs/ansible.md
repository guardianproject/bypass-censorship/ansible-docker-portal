<!-- markdownlint-disable -->
## Role Variables

| Variable     | Default Value  | Description  |
| ------------ | -------------- | ------------ |
| bc_portal_configuration | **REQUIRED** | The configuration to be provided to the portal container. See https://bypass.censorship.guide/admin/conf.html for the available configuration values. |
| bc_portal_debug | `false` | Enable debugging features. These are expected to be safe to use in a production environment but do increase the attack surface and so their usage should be limited to active troubleshooting only. |
| bc_portal_flask_secret | **REQUIRED** | A randomly generated string to use as a salt within the Flask application. |
| bc_portal_matrix_api_key | **REQUIRED** | A randomly generated string to use as an API key internally. |
| bc_portal_matrix_id | **REQUIRED** | Matrix ID for user to use for notifications. |
| bc_portal_matrix_password | **REQUIRED** | Matrix password for user to use for notifications. |
| bc_portal_namespace | **REQUIRED** | A short (3-5 charachter) string that uniquely identifies this portal deployment. |
| bc_portal_portal_image | `registry.gitlab.com/guardianproject/bypass-censorship/portal:latest` | The Docker image to use for the portal container. |
| bc_portal_postgres_image | `postgres:14` | The Docker image to use for the PostgreSQL container. |
| bc_portal_postgresql_database | `bc` | The database name for the portal database in PostgreSQL. |
| bc_portal_postgresql_password | **REQUIRED** | The password for the portal database in PostgreSQL. |
| bc_portal_postgresql_user | `bc` | The username for the portal database in PostgreSQL. |

<!-- markdownlint-enable -->
